package com.regionservice.client;

import com.regionservice.model.Regions;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "admitadClient", url = "https://api.admitad.com")
public interface AdmitadContentClient {

    @GetMapping(value = "/websites/regions/?limit=${limit}")
    Regions regions();
}