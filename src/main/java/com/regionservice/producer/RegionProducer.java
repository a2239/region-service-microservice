package com.regionservice.producer;

import com.regionservice.client.AdmitadContentClient;
import com.regionservice.model.Regions;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Slf4j
@Component
@AllArgsConstructor
public class RegionProducer {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    private final AdmitadContentClient admitadContentClient;

    public void regionProduce() {
        log.info("RegionProducer regionProduce begin send message");
        Regions regions = admitadContentClient.regions();
        kafkaTemplate.send("regions-topic", regions);
        log.info("RegionProducer regionProduce message have sent regions - {}", regions);
    }
}
