package com.regionservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.PropertySource;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@PropertySource(value = "classpath:properties/admitad.yaml")
@EnableFeignClients
@EnableKafka
public class RegionServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RegionServiceApplication.class, args);
	}

}
